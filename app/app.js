require("./utils/db");
const express = require('express');
const cors = require('cors');
const config = require('./config/config');
const mainRoutes = require('./routes/mainRoutes');
// const mainController=require("./controllers/mainController");


const app = express();

// Middleware
app.use(cors());
app.use(express.json());

// Routes
app.use('/', mainRoutes);

// Start server
app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`);
});
