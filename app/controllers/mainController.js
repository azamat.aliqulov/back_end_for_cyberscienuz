const TableModel = require('../models/tableModel');
const PDFDocument = require('pdfkit');
const fs = require('fs');
const moment = require('moment');

exports.getTables = async (req, res) => {
  try {
    const data = await TableModel.find();

    res.status(200).json({
      success: true,
      message: 'Tables retrieved successfully',
      tables: data
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      message: 'Error retrieving tables',
      error: error
    });
  }
};

exports.deleteTables = async (req, res) => {
  try {
    const data = await TableModel.deleteMany();

    res.status(200).json({
      success: true,
      message: 'Tables deleted successfully',
      tables: data
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      message: 'Error deleting tables',
      error: error
    });
  }
};

exports.postNameAndEmail = async (req, res) => {
  try {
    const { name, email } = req.body;

    await TableModel.create({
      name,
      email,
    });

    res.status(201).json({
      success: true,
      message: 'Name and email saved to database.',
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      message: 'Unable to save name and email to database.',
      error: error
    });
  }
};

exports.exportToPdf = async (req, res) => {
  try {
    const tableData = await TableModel.find();

    const doc = new PDFDocument();
    const stream = doc.pipe(fs.createWriteStream('table-data.pdf'));

    doc.fontSize(12).text('Table Data', { align: 'center' }).moveDown();

    doc.fontSize(10);
    tableData.forEach((data, index) => {
      doc.text(`${index + 1}. Ism Sharif: ${data.name}, Phone: ${data.email}, Murjoat qoldirilgan sana: ${data.created_at}`).moveDown();
    });

    doc.end();
    stream.on('finish', () => {
      res.status(201).download('table-data.pdf');
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      message: 'Unable to export table data to PDF.',
    });
  }
};

