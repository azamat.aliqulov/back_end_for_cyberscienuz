const express = require('express');
const mainController = require('../controllers/mainController');

const router = express.Router();

router.post('/data', mainController.postNameAndEmail);
router.get('/tables', mainController.getTables);
router.get('/export-to-pdf', mainController.exportToPdf);
router.get('/deleteTables', mainController.deleteTables);


module.exports = router;
