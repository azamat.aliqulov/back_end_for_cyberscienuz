const mongoose = require('mongoose');

// Replace the following line with your MongoDB connection string
const dbURI = `mongodb://db:27017/nodeapp`;

mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Database connected'))
  .catch(err => console.error('Database connection error:', err));

// module.exports = mongoose.connection;
