#!/bin/bash

mkdir -p app/controllers
touch app/controllers/mainController.js

mkdir -p app/models
touch app/models/tableModel.js

mkdir -p app/routes
touch app/routes/mainRoutes.js

mkdir -p app/utils
touch app/utils/db.js

touch app.js

mkdir -p config
touch config/config.js

npm init -y

touch README.md